const { TOKEN } = require("dotenv").config()?.parsed;
const db = require("./db");
const axios = require("axios").default;
const { format } = require("prettier");
const Discord = require("discord.js");
const client = new Discord.Client();
client.login(TOKEN);
let MENTION;
client.once("ready", () => {
	MENTION = new RegExp("^<@!{0,1}" + client.user.id + ">"); // mobile users
	console.log("BOT ACTIVE");
	console.log("Tag: " + client.user.tag);
	console.log("Mention: " + MENTION);
	client.user.setActivity({
		name: "omg neil ciciereaga",
		url: "https://www.youtube.com/watch?v=O_arvS_7HF0",
		type: "STREAMING",
	});
});
client.on("message", async msg => {
	if (msg.author.bot) return;
	if (!msg.guild) return;
	if (!msg.channel.permissionsFor(client.user).has("SEND_MESSAGES")) return;
	let info = db.get.get(msg.guild.id);
	if (!info) {
		db.add.run(msg.guild.id, "");
		info = db.get.get(msg.guild.id);
	}
	console.log(info);
	if (msg.content.match(MENTION)) {
		// commands :trollpixel:
		const args = msg.content.replace(/ +/g, " ").split(" ");
		args.shift(); // shift mention
		const command = args.shift(); // get command
		console.log(command, args);
		switch (command) {
			case "channel":
				if (args[0] && msg.member.hasPermission("MANAGE_CHANNELS")) {
					let channel = msg.guild.channels.cache.get(args[0]);
					console.log(channel);
					if (channel)
						if (channel.type === "text") {
							db.update.run(args[0], msg.guild.id);
							msg.reply("listen channel set to <#" + args[0] + ">");
						} else msg.reply("you can't use a voice channel for listening");
					else
						msg.reply("that channel does not exist (please supply a valid ID)");
				} else {
					if (info.channel_id)
						msg.reply("listen channel for this guild is " + info.channel_id);
					else msg.reply("no listen channel is set for this guild (!)");
				}
				break;
			case "clear":
				if (msg.member.hasPermission("MANAGE_CHANNELS")) {
					let channel = msg.guild.channels.cache.get(info.channel_id);
					if (channel) {
						let pID = channel.parentID;
						msg.guild.channels.cache.each(ch => {
							if (ch.parentID === pID && ch.type === "voice")
								ch.delete("Clearing channels");
						});
						msg.reply("cleared voice channels in listen channel's category");
					} else msg.reply("couldn't find listen channel");
				} else
					msg.reply(
						"you don't have the required permission(s) to use this command (Manage Channels)"
					);
				break;
			case "config":
				if (
					msg.attachments.size > 0 &&
					msg.member.hasPermission("MANAGE_CHANNELS")
				) {
					let file = msg.attachments.first();
					if (!file.name.endsWith(".json"))
						return msg.reply("config must be a JSON file");
					const { status, data } = await axios.get(file.url);
					if (status !== 200)
						return msg.reply("file removed from Discord servers");
					if (typeof data !== "object") return msg.reply("invalid JSON data");
					if (typeof data.channel !== "string")
						return msg.reply("malformed config");
					if (!msg.guild.channels.cache.has(data.channel))
						return msg.reply(
							"that channel does not exist (please supply a valid ID)"
						);
					db.remove.run(msg.guild.id);
					db.add.run(msg.guild.id, data.channel);
					msg.reply("parsed config");
				} else {
					let attachment = new Discord.MessageAttachment(
						Buffer.from(
							format(
								JSON.stringify({
									channel: info.channel_id || "",
								}),
								{ parser: "json-stringify" }
							)
						),
						"config.json"
					);
					msg.reply("here's the current config", attachment);
				}
				break;
			case "help":
				msg.reply(
					new Discord.MessageEmbed()
						.setTitle("Commands Help")
						.setDescription("To use commands, ping me")
						.addFields([
							{
								name: "channel",
								value: `Set/display the listen channel (all messages in this channel will create voice channels)
**Usage**: channel [id]
**Permissions**: Manage Channels (to set)`,
							},
							{
								name: "clear",
								value: `Delete all voice channels in the category of the listen channel
**Usage**: clear
**Permissions**: Manage Channels`,
							},
							{
								name: "config",
								value: `Set/display the config in a JSON file
**Usage**: config
**Permissions**: Manage Channels (to set)`,
							},
						])
						.setColor(0x00ffff)
				);
				break;
			default:
				msg.reply(
					`pong! Use the help command (${MENTION} help) to get more information.`
				);
				break;
		}
	} else if (msg.channel.id === info.channel_id) {
		console.log("## CREATING CHANNELS FOR GUILD ID " + msg.guild.id + " ##");
		const channelNames = msg.content
			.split("\n")
			.map(str => str.trim())
			.filter(str => str !== "")
			.map(str => str.substr(0, 100));
		if (channelNames.length === 0) return;
		console.log(channelNames);
		msg.channel.startTyping();
		/** @type {Discord.Channel[]} */
		const channels = channelNames.map(cName =>
			msg.guild.channels.create(cName, {
				type: "voice",
				parent: msg.channel.parent,
			})
		);
		let list = "";
		let ignoreErrors = false;
		for (i in channels) {
			try {
				channels[i] = await channels[i];
				list += `<#${channels[i].id}>\n`;
			} catch (err) {
				if (!ignoreErrors) {
					if (err.code === 50035) msg.reply("channel limit of 50 hit! (50035)");
					else
						return msg.reply(
							"an unknown error occured! (" + err.code || "Unknown code" + ")"
						);
					ignoreErrors = true;
				}
			}
		}
		list += "";
		msg.channel.stopTyping();
		if (list) msg.reply("created channels: ```\n" + list + "```");
	}
});
