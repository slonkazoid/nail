const database = require("better-sqlite3")("data.db");
database.exec(`CREATE TABLE IF NOT EXISTS guilds (
    guild_id TEXT NOT NULL,
	channel_id TEXT
);`);
module.exports = {
	db: database,
	add: database.prepare(
		"INSERT INTO guilds (guild_id, channel_id) VALUES (?, ?)"
	),
	remove: database.prepare("DELETE FROM guilds WHERE guild_id = ?"),
	get: database.prepare("SELECT * FROM guilds WHERE guild_id = ?"),
	update: database.prepare(
		"UPDATE guilds SET channel_id = ? WHERE guild_id = ?"
	),
};
